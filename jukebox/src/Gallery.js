import React, { Component } from 'react';
import './App.css';

class Gallery extends Component {

    componentDidMount() {
        console.log('Gallery');
    }

    componentWillUnmount() {
        console.log('Gallery unmount');
    }

    render() {
        if (this.props.tracks === null) {
            return null;
        }
        let tracks = this.props.tracks !== undefined ? this.props.tracks : [];
    
        return (
          <div className="gallery">
            {
              tracks.map((track, k) => {
                let trackImg = track.album.images[0].url;
                return (
                  <div key={k} className="track">
                    <img
                      src={trackImg}
                      className="track-img"
                      alt="track"
                    />
                    <p className="track-text">
                      {track.name}
                    </p>
                  </div>
                )
              })
            }
          </div>
        )
      }
}

export default Gallery;