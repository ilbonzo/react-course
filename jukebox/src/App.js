import React, { Component } from 'react';
import './App.css';
import { FormGroup, FormControl, InputGroup, Glyphicon} from 'react-bootstrap';

import Config from './config.json';
import Profile from './Profile';
import Gallery from './Gallery';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        query: '',
        artist: null,
        tracks: null
    }
}

search() {
  console.log(Config);
  const token = Config.access_token_bearer;
  // console.log('this.state', this.state);
  const BASE_URL = 'https://api.spotify.com/v1/search';
  const ALBUM_URL = 'https://api.spotify.com/v1/artists';

  let FETCH_URL = `${BASE_URL}?q=${this.state.query}&type=artist&limit=1`;

  console.log(FETCH_URL);

  fetch(FETCH_URL, {
    method: 'GET',
    mode: 'cors', 
    redirect: 'follow',
    headers: {
        'Authorization': 'Bearer ' + token
    }
    }).then( response => response.json())
    .then(json => {
      console.log('json', json);
      const artist = json.artists.items[0]
      this.setState({artist});

      FETCH_URL = `${ALBUM_URL}/${artist.id}/top-tracks?country=US&`;
      fetch(FETCH_URL, {
          method: 'GET',
          mode: 'cors', 
          redirect: 'follow',
          headers: {
              'Authorization': 'Bearer ' + token
          }
    })
    .then((response) => response.json())
    .then((responseJSON) => {
        let tracks = responseJSON.tracks;
        console.log('tracks', tracks);
        this.setState({tracks});
    })
  })
}

render() {
    console.log('----- render');
    // let profile = '';
    // let gallery  = '';
    // if (this.state.query !== '') {
    //     profile = <Profile artist={this.state.artist}/>
    //     gallery = <Gallery />
    // } 
    return (
        <div className="app">
            <div className="app-title">Music Master</div>
            <FormGroup>
                <InputGroup>
                    <FormControl
                    type="text"
                    placeholder="Search for an Artist"
                    value={this.state.query}
                    onChange={ event => {this.setState({query: event.target.value})}}
                    onKeyPress={event => {event.key === 'Enter' ? this.search() : console.log(event.key)}}
                    />
                    <InputGroup.Addon onClick={() => this.search()}>                        
                        <Glyphicon glyph="search" />
                    </InputGroup.Addon>
                </InputGroup>
            </FormGroup>

            {/* {profile}
            {gallery} */}
            {
              this.state.artist !== null ?
                <div>
                    <Profile artist={this.state.artist}/> 
                    <Gallery tracks={this.state.tracks}/>
                </div>
                :
                <div></div>
            }

          
        </div>
    )
  }
}

export default App;
