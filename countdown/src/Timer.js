import React, { Component } from 'react';
import { Form, FormControl, Button } from 'react-bootstrap';

class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hours: 0,
            minutes: 0,
            seconds: 0,
            totalSeconds: 0,
            intervalId: null
        }
    }

    componentWillMount() {
        // console.log('Component will mount');
    }

    componentDidMount() {
        // console.log('Component did mount');
    }

    componentWillUnmount() {
       clearInterval(this.state.intervalId);
    }

    leadingZero(num) {
        return num < 10 ? '0' + num : num;
    }

    getTimeUntil() {
        const hoursInSeconds = parseInt(this.state.hours, 10) * 60 * 60;
        const minutesInSeconds = parseInt(this.state.minutes, 10) * 60;
        const secondsInSeconds = parseInt(this.state.seconds, 10);
        const totalSeconds = hoursInSeconds + minutesInSeconds + secondsInSeconds;

        const newTotalSeconds = totalSeconds - 1;

        if (newTotalSeconds === -1) {
            this.stopTimer();
            alert('Time finish');
            return;
        }

        const seconds = Math.floor((newTotalSeconds) % 60);
        const minutes = Math.floor((newTotalSeconds/60) % 60);
        const hours = Math.floor((newTotalSeconds/(60 * 60)) % 24);

        this.setState({hours: hours, minutes: minutes, seconds: seconds, totalSeconds: newTotalSeconds});
    }

    startTimer() {
        // this.setState({
        //     hours: $('#hours').val(),
        // });
        var intervalId = setInterval(() => this.getTimeUntil(), 1000);
        this.setState({intervalId: intervalId});
    }

    stopTimer() {
        clearInterval(this.state.intervalId);
    }

    render() {
        return (
            <div className="timer">
                <div>
                    <div className="timer-hours">{this.leadingZero(this.state.hours)} hours</div>
                    <div className="timer-minutes">{this.leadingZero(this.state.minutes)} minutes</div>
                    <div className="timer-seconds">{this.leadingZero(this.state.seconds)} seconds</div>
                </div>
                <Form inline>
                    <FormControl
                        className="timer-input timer-input-hours"
                        placeholder="00"
                        onChange={event => this.setState({hours: event.target.value})}
                    />
                    <span>h</span>
                    <FormControl
                        className="timer-input timer-input-minutes"
                        placeholder="00"
                        onChange={event => this.setState({minutes: event.target.value})}
                    />
                    <span>m</span>
                    <FormControl
                        className="timer-input timer-input-seconds"
                        placeholder="00"
                        onChange={event => this.setState({seconds: event.target.value})}
                    />
                    <span>s</span>
                    <Button
                        onClick={() => this.startTimer()}
                    >Start</Button>
                    <Button
                        onClick={() => this.stopTimer()}
                    >Stop</Button>
                </Form>
            </div>
        );
    }
}

export default Timer;
