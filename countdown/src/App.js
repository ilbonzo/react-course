import React, { Component } from 'react';

import { Form, FormControl, Button } from 'react-bootstrap';
import Clock from './Clock';
import Timer from './Timer';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deadline: 'December 25, 2018',
            newDeadline: ''
        }
    }

    changeDeadLine() {
        this.setState({
            deadline: this.state.newDeadline
        })
    }

    render() {
        return (
            <div>
                <div>
                    Countdown to {this.state.deadline}
                </div>
                <Clock deadline={this.state.deadline}/>
                {/* <div>
                    <input
                        onChange={event => this.setState({ newDeadline: event.target.value })}
                    />
                    <button onClick={() => this.changeDeadLine()}>Submit</button>
                </div> */}
                <Form inline>
                    <FormControl
                        className="deadline-input"
                        placeholder="new date"
                        onChange={event => this.setState({newDeadLine: event.target.value})}
                    />
                    <Button onClick={() => this.changeDeadLine()}>Submit</Button>
                </Form>

                <hr />
                <div className="stop-watch-title">
                    StopWatch to 
                </div>
                <Timer />
            </div>
        )
    }
}

export default App;