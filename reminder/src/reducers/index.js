import { ADD_REMINDER, DELETE_REMINDER, CLEAR_REMINDERS } from '../constants';

const reminder = (action) => {

    let { text } = action;
    // let text = action.text;

    return {
      id: Math.random(),
      text
    }
}

const removeById = (state = [], id) => {
    const reminders = state.filter(reminder => {
      return (
        reminder.id !== id
      )
    });
    return reminders;
}

  
const reminders = (state = [], action) => {
    let reminders = null;
    switch (action.type) {
      case ADD_REMINDER:
        reminders = [...state, reminder(action)]
        return reminders
      case DELETE_REMINDER:
          reminders = removeById(state, action.id)
          return reminders
      default:
        return state
    }
}

export default reminders;