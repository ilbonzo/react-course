import { ADD_REMINDER, DELETE_REMINDER, CLEAR_REMINDERS } from '../constants';

// Actions creators
export const addReminder = (text) => {
    return {
      type: ADD_REMINDER,
      text, // text: text
    }
}

// Actions creators
export const deleteReminder = (id) => {
    return {
      type: DELETE_REMINDER,
      id, // id: id
    }
}