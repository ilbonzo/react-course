import React, { Component } from 'react';
// use the conenct function to map the state to props
import { connect } from 'react-redux';
import  { addReminder, deleteReminder } from './actions';


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
        }
    }

    addReminder() {
        // fare dispatch della action
        // console.log("this.state", this.state);
        // console.log("this.props", this.props);
        this.props.addReminder(this.state.text);
    }

    deleteReminder(id) {
        this.props.deleteReminder(id);
    }

    renderReminders() {
        const reminders = this.props.reminders;
        return (
          <ul className="list-group col-sm-4">
          {
            reminders.map((reminder) => {
              return (
                <li key={reminder.id} className="list-group-item">
                  <div className="list-item">
                    <div>{reminder.text}</div>
                    <div
                      onClick={() => this.deleteReminder(reminder.id)}
                      className="delete-button list-item"
                    >
                      &#x2715;
                    </div>
                  </div>
                </li>
              )
            })
          }
          </ul>
        )
      }

  render() {
    console.log(this.props);
    return (
      <div className="app container-fluid">
          <div className="title">
            Reminder
          </div>
          <form className="form-inline">
            <div className="form-group">
              <input
                placeholder='I have to...' 
                className="form-control"
                onChange={event => this.setState({text: event.target.value})}
              />
            </div>
            <div>
              <button 
                type="button" 
                className="btn btn-success"
                onClick={() => this.addReminder()}
              >
                Add Reminder
              </button>
              </div>
          </form>
          {this.renderReminders()}
        </div>
    );
  }
}

// Prima facevo così
// export default App;

function mapStateToProps(state) {
    console.log(state);
    return {
        reminders: state
    }
}
  
  
export default connect(mapStateToProps, { addReminder, deleteReminder })(App);